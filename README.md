# _Syncore_

## Чем занимается?

> Добавляет зависимости используемые в элеменьах на типовых сайтах.

## Требования к платформе

- _Drupal 8-11_
- _PHP 7.4.0+_

## устанавливаемые модули

- [maxlength] (https://www.drupal.org/project/maxlength)
- [imageapi optimize] (https://www.drupal.org/project/imageapi_optimize)
- [crop] (https://www.drupal.org/project/crop)
- [focal point] (https://www.drupal.org/project/focal_point)
- [paragraphs previewer] (https://www.drupal.org/project/paragraphs_previewer)
- [responsive preview] (https://www.drupal.org/project/responsive_preview)
- [entity clone] (https://www.drupal.org/project/entity_clone)
- [coffee] (https://www.drupal.org/project/coffee)
- [search api] (https://www.drupal.org/project/search_api)
- [easy breadcrumbs] (https://www.drupal.org/project/easy_breadcrumb)
